import "dart:async";

import "package:dartx/dartx.dart";
import "package:multiple_change_notifiers/multiple_change_notifiers.dart";
import "package:random_string/random_string.dart";
import "package:test/test.dart";

void main() {
  group("Tests for EmptyValueNotifier<String>:", () {
    test("Add 1 valueListener and listen for a result.", () async {
      const updateValue = "I Like Cookies";

      final completer = Completer<String>();
      final notifier = EmptyValueNotifier<String>.empty();

      void valueListener(String value) {
        completer.complete(value);
      }

      notifier
        ..addValueListener(valueListener)
        ..value = updateValue;

      expect(await completer.future, updateValue);

      notifier.removeValueListener(valueListener);
    });

    test(
      "Instantiate an EmptyValueNotifier.empty() "
      "and read it's value without setting it. "
      "This should throw a StateError.",
      () async {
        final notifier = EmptyValueNotifier<String>.empty();
        expect(
          // We have to use a function instead of just the getter
          // for our actual value, due to the way throwsA detects
          // an Error.
          () => notifier.value,
          throwsA(isA<StateError>()),
        );
      },
    );

    test("Calling toString on an empty EmptyValueNotifier.", () {
      EmptyValueNotifier<String>.empty().toString();
    });

    test("Add 1 value listener and listen for a result.", () async {
      const updateValue = "I Like Cookies";

      final completer = Completer<String>();
      final notifier = EmptyValueNotifier("initial");

      void valueListener(String value) {
        completer.complete(value);
      }

      notifier
        ..addValueListener(valueListener)
        ..value = updateValue;

      expect(await completer.future, updateValue);

      notifier.removeValueListener(valueListener);
    });

    test("Add 10 value listener and listen for a result.", () async {
      const result = "I Like Cookies";

      final notifier = EmptyValueNotifier("initial");
      final listeners = List.generate(10, (index) => Completer<String>())
          .associateWith((e) => e.complete)
        ..forEach((key, value) => notifier.addValueListener(value));

      notifier.value = result;

      for (final item in listeners.keys) expect(await item.future, result);

      listeners.forEach((key, value) => notifier.removeValueListener(value));
    });

    test("Add 10 value listener and listen for 50 results.", () async {
      final notifier = EmptyValueNotifier("initial");

      for (var i = 0; i < 50; i++) {
        final result = randomString(10);

        // Gotta create listeners each iteration, because
        // a Completer is useless after Completer.complete has been
        // called.
        final listeners = List.generate(10, (index) => Completer<String>())
            .associateWith((e) => e.complete)
          ..forEach((key, value) => notifier.addValueListener(value));

        notifier.value = result;

        for (final completer in listeners.keys)
          expect(await completer.future, result);

        listeners.forEach((key, value) => notifier.removeValueListener(value));
      }
    });

    test(
        "Add 10 value listener and 10 normal listener "
        "and listen for 50 results.", () async {
      final notifier = EmptyValueNotifier("initial");

      for (var i = 0; i < 50; i++) {
        final result = randomString(10);

        // Gotta create listeners each iteration, because
        // a Completer is useless after Completer.complete has been
        // called.
        final valueListeners = List.generate(10, (index) => Completer<String>())
            .associateWith((e) => e.complete)
          ..forEach((key, value) => notifier.addValueListener(value));
        final normalListeners =
            List.generate(10, (index) => Completer<String>())
                .associateWith((e) => () => e.complete(notifier.value))
              ..forEach((key, value) => notifier.addListener(value));

        notifier.value = result;

        for (final completer in [
          ...valueListeners.keys,
          ...normalListeners.keys,
        ]) expect(await completer.future, result);

        valueListeners
            .forEach((key, value) => notifier.removeValueListener(value));
        normalListeners.forEach((key, value) => notifier.removeListener(value));
      }
    });
  });
}
