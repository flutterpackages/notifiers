import "dart:async";
import "dart:math";

import "package:multiple_change_notifiers/multiple_change_notifiers.dart";
import "package:test/test.dart";

void main() {
  group("Tests for StreamSubscriptionNotifier<int>:", () {
    late StreamController<int> controller;
    late StreamSubscriptionNotifier<int> notifier;
    const numbersLength = 20;
    late List<int> numbers;

    setUp(() {
      controller = StreamController();
      notifier = StreamSubscriptionNotifier(controller.stream.sub);

      numbers = List.generate(
        numbersLength,
        (_) => Random().nextInt(98745632),
      );
    });

    tearDown(() async {
      await controller.close();

      expect(notifier.isDone.value, true);

      await notifier.dispose();
    });

    test("Emit $numbersLength random numbers.", () async {
      var valueIndex = 0;
      var asyncIndex0 = 0;
      var asyncIndex1 = 0;

      notifier
        ..addListener(
          expectAsync0(
            () => expect(notifier.value, numbers[asyncIndex0++]),
            count: numbers.length,
          ),
        )
        ..addValueListener(
          expectAsync1(
            (v) => expect(v, numbers[asyncIndex1++]),
            count: numbers.length,
          ),
        );

      for (valueIndex = 0; valueIndex < numbers.length; valueIndex++)
        controller.add(numbers[valueIndex]);
    });
  });

  group("Tests for StreamSubscriptionNotifier<int> with error:", () {
    late Object error;
    late StreamController<int> controller;
    late StreamSubscriptionNotifier<int> notifier;
    const numbersLength = 20;
    late List<int> numbers;

    setUp(() {
      error = "MyError";
      controller = StreamController();
      notifier = StreamSubscriptionNotifier(
        controller.stream.sub,
      )..onError.addValueListener((e) {
          expect(e.error, error);
          expect(notifier.hadError, true);
          expect(notifier.isDone.value, false);
          expect(notifier.isCancelled.value, false);
        });

      numbers = List.generate(
        numbersLength,
        (_) => Random().nextInt(98745632),
      );
    });

    tearDown(() async {
      await controller.close();

      expect(notifier.hadError, true);

      await notifier.dispose();

      expect(notifier.hadError, true);
      expect(notifier.isDone.value, false);
      expect(notifier.isCancelled.value, false);
    });

    test("Emit $numbersLength random numbers.", () async {
      var valueIndex = 0;
      var asyncIndex0 = 0;
      var asyncIndex1 = 0;

      notifier
        ..addListener(
          expectAsync0(
            () => expect(notifier.value, numbers[asyncIndex0++]),
            count: numbers.length,
          ),
        )
        ..addValueListener(
          expectAsync1(
            (v) => expect(v, numbers[asyncIndex1++]),
            count: numbers.length,
          ),
        );

      for (valueIndex = 0; valueIndex < numbers.length; valueIndex++)
        controller.add(numbers[valueIndex]);

      controller.addError(error);
    });
  });
}
