part of "../multiple_change_notifiers.dart";

extension SubExtension<T> on Stream<T> {
  /// Get a [StreamSubscription] with an empty StreamSubscription.onData]
  /// handler of this [Stream].
  StreamSubscription<T> get sub => listen(
        (_) {},
        cancelOnError: false,
      );
}
