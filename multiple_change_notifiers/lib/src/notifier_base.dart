part of "../multiple_change_notifiers.dart";

abstract class NotifierBase<T> extends ChangeNotifier
    with AdvancedValueListenableMixin<T>, DisposableMixin
    implements AdvancedValueListenable<T> {}

abstract class ValueNotifierBase<T> extends NotifierBase<T>
    implements ValueNotifier<T> {
  @override
  T get value;

  @override
  set value(T newValue);
}
