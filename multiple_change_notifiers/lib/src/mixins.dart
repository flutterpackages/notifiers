part of "../multiple_change_notifiers.dart";

mixin DisposableMixin {
  /// {@template src.disposable.dipose}
  /// Discards any resources used by the object. After this is called, the
  /// object is not in a usable state and should be discarded (calls to
  /// addListener, addValueListener, removeListener and removeValueListener)
  /// will throw after the object is disposed).
  ///
  /// This method should only be called by the object's owner.
  /// {@endtemplate}
  void dispose();
}

mixin ValueComparatorMixin<T> on ValueNotifierBase<T> {
  /// A custom comparator that can be used to determine if a new value
  /// for [value] is equal to the current [value] or not. This is used in
  /// the setter for [value] and if the new value is equal to the current
  /// one, then the current value will not be changed and therefore no
  /// listeners will be notified.
  ///
  /// By default, objects are compared using the `==` operator.
  EqualityComparator<T>? get comparator;

  /// Returns true if [other] is equal to [value], otherwise
  /// false.
  ///
  /// If [comparator] is non-null, then it is used for the comparison,
  /// otherwise the `==` operator gets used.
  bool _compare(T value, T other) {
    if (null != comparator) return comparator!(value, other);
    return value == other;
  }
}
