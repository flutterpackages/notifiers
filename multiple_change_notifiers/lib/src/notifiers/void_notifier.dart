part of "../../multiple_change_notifiers.dart";

/// A [VoidNotifier] is a [ChangeNotifier] that wraps no value.
class VoidNotifier extends ChangeNotifier {
  /// This method exposes the [ChangeNotifier.notifyListeners] method,
  /// which should normally not get called on its' own, because it is
  /// a protected method.
  void notify() => notifyListeners();
}
