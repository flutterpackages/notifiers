part of "../../multiple_change_notifiers.dart";

/// A [ChangeNotifier] that holds a single value.
///
/// When [value] is replaced with something that is not equal to the old
/// value as evaluated by the equality operator ==, this class notifies its
/// listeners.
class EmptyValueNotifier<T> extends ValueNotifierBase<T>
    with ValueComparatorMixin<T>
    implements EmptyValueListenable<T> {
  /// Creates a [ChangeNotifier] that wraps this [value].
  EmptyValueNotifier(
    this._value, {
    this.comparator,
  }) : _isEmpty = false;

  EmptyValueNotifier.empty({
    this.comparator,
  }) : _isEmpty = true;

  @override
  final EqualityComparator<T>? comparator;

  late T _value;

  // We have to use a flag in the costructor to indicate that
  // this EmptyValueNotifier is indeed empty, only checking
  // if the null == value, is not enough because null may be
  // a valid value.
  bool _isEmpty;

  @override
  bool get isEmpty => _isEmpty;

  @override
  bool get isNotEmpty => !isEmpty;

  /// {@template src.empty.empty_value_notifier.value}
  /// The current value stored in this notifier.
  ///
  /// When the value is replaced with something that is not equal to the old
  /// value as evaluated by the equality operator ==, this class notifies its
  /// listeners.
  ///
  /// Throws a [StateError], when trying to read this value and
  /// [isEmpty] is `true`.
  /// {@endtemplate}
  @override
  T get value {
    if (isEmpty)
      throw StateError(
        "Cannot access the value of this EmptyValueNotifier, "
        "because isEmpty is true. "
        "The value has to be set before accessing it, "
        "if the constructor EmptyValueNotifier.empty was used.",
      );

    return _value!;
  }

  @override
  set value(T newValue) {
    // Return if this notifier is not empty and the new
    // value is equal to the current one.
    if (isNotEmpty && _compare(_value, newValue)) return;

    _isEmpty = false;

    _value = newValue;
    notifyListeners();
  }

  @override
  String toString() =>
      "${describeIdentity(this)}(${isEmpty ? "<empty>" : _value})";
}

abstract class EmptyValueListenable<T> extends AdvancedValueListenable<T> {
  /// {@template src.empty.empty_value_listenable.is_empty}
  /// `true` if the [value] does not contain a valid value,
  /// because it has been initialized by the [EmptyValueNotifier.empty]
  /// constructor.
  ///
  ///
  /// `false` if the default constructor of [EmptyValueNotifier] was used,
  /// or the value has been set at least once.
  /// {@endtemplate}
  bool get isEmpty;

  /// {@template src.empty.empty_value_listenable.is_not_empty}
  /// `false` if the [value] does not contain a valid value,
  /// because it has been initialized by the [EmptyValueNotifier.empty]
  /// constructor.
  ///
  ///
  /// `true` if the default constructor of [EmptyValueNotifier] was used,
  /// or the value has been set at least once.
  /// {@endtemplate}
  bool get isNotEmpty;
}
