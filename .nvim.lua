if nil == AddOnPluginScriptsLoadedHandler then return end

AddOnPluginScriptsLoadedHandler(function()
  if nil == LspSetupDart then
    vim.notify(
      "There is no LspSetupDart function",
      vim.log.levels.WARN
    )
    return
  end
  LspSetupDart()
end)
