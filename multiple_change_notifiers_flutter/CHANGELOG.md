# 2.1.1

  * Changed project structure.

# 2.1.0

  * Renamed function `initializeMultipleChangeNotifiers` to `setMultipleChangeNotifiersLibConfig` and removed requirement for the function to be called before the library can be used.

# 2.0.0

  * Bumped Dart SDK version constraint up to `>=3.3.0 <4.0.0`

  * Added global function `initializeMultipleChangeNotifiers`. This function needs to be called before the `multiple_change_notifiers` library can be used.

# 1.0.11

  * Removed dependency on Flutter SDK.

# 1.0.10

  * Added the mixin `ValueComparatorMixin` with adds the `comparator` property to a class. This property is a function that can be used to replace the default comparison of `==` to check for different values. This mixin has been mixed into the `AdvancedValueNotifier` and `EmptyValueNotifier` classes.

# 1.0.9

  * Added `VoidNotifier` class.

# 1.0.8

  * Changed package name from `notifiers` to `multiple_change_notifiers`.

# 1.0.7

  * Updated [analysis_options.yaml](analysis_options.yaml).
  
  * Made package ready to get published on [pub.dev](https://pub.dev)

# 1.0.6

  * Fixed error caused by `KeyValueNotifier.dispose()`:
    
    ```
    Unsupported operation: Cannot clear unmodifiable Map
    ```

# 1.0.5
  
  * Added **[disposable.dart](lib/src/disposable.dart)**.

# 1.0.4

  * Added **[key_value_notifier.dart](lib/src/key/key_value_notifier.dart)**.

  * Added **[key_value_listenable.dart](lib/src/key/key_value_listenable.dart)**.

# 1.0.3

  * Modified `StreamSubscriptionNotifier` in **[stream_subscription_notifier.dart](lib/src/stream_subscription/stream_subscription_notifier.dart)**.

  * Added **[stream_error.dart](lib/src/models/stream_error.dart)**.

# 1.0.2

  * Added **[stream_subscription_notifier.dart](lib/src/stream_subscription/stream_subscription_notifier.dart)**.

# 1.0.1

  * Fixed a bug where calling `EmptyValueNotifier.toString()`, while `EmptyValueNotifier.isEmpty` is `true`, threw a `StateError`.

# 1.0.0

  * Added **[advanced_value_listenable.dart](lib/src/advanced_value_listenable.dart)**.
   
  * Added **[advanced_value_notifier.dart](lib/src/advanced_value_notifier.dart)**.
  
  * Added **[empty_value_listenable.dart](lib/src/empty_value_listenable.dart)**.
    
  * Added **[empty_value_notifier.dart](lib/src/empty_value_notifier.dart)**.

  * Added **[typedefs.dart](lib/src/typedefs.dart)**.

  * Added **[advanced_value_notifier_test.dart](test/advanced_value_notifier_test.dart)**.

  * Added **[empty_value_notifier_test.dart](test/empty_value_notifier_test.dart)**.

# 0.0.1

  * initial release.
