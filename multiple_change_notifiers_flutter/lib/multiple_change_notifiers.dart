library multiple_change_notifiers_flutter;

import "dart:async";
import "dart:collection";

import "package:meta/meta.dart";

import "src/flutter_sdk/change_notifier.dart";
import "src/flutter_sdk/diagnostics.dart";
import "src/flutter_sdk_config.dart";

export "src/flutter_sdk/change_notifier.dart"
    show ChangeNotifier, Listenable, ValueListenable, ValueNotifier;

export "src/flutter_sdk_config.dart" show DartSdkLibConfig, FlutterSdkLibConfig;

part "src/extensions.dart";
part "src/mixins.dart";
part "src/models/stream_error.dart";
part "src/notifier_base.dart";
part "src/notifiers/advanced_value_notifier.dart";
part "src/notifiers/empty_value_notifier.dart";
part "src/notifiers/key_value_notifier.dart";
part "src/notifiers/stream_subscription_notifier.dart";
part "src/notifiers/void_notifier.dart";
part "src/typedefs.dart";

/// Change the [LibConfig] for the `multiple_change_notifiers` library.
///
/// This package uses some parts of the Flutter SDK. The necessary source code
/// was copied from the Flutter SDK and pasted into this package. Nearly all
/// parts of the Flutter SDK heavily depend on each other, so only the required
/// code was copied and the parts that are not really necessary for the
/// functionality of said code were removed and replaced with the functions and
/// members from [LibConfig]. By default the [LibConfig] is set to
/// `const DartSdkLibConfig()`.
///
/// When using this package with the Flutter SDK, you should use this function
/// to set the [LibConfig] to [FlutterSdkLibConfig].
///
/// * See [DartSdkLibConfig]
/// * See [FlutterSdkLibConfig]
void setMultipleChangeNotifiersLibConfig(LibConfig args) {
  FlutterSdkUtils.instance.args = args;
}
