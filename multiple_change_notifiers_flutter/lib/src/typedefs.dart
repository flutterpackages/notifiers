part of "../multiple_change_notifiers.dart";

typedef ValueCallback<T> = void Function(T value);

typedef EqualityComparator<T> = bool Function(T a, T b);
