part of "../../multiple_change_notifiers.dart";

@immutable
final class StreamError {
  const StreamError._({
    required this.error,
    required this.stackTrace,
  });

  final Object error;
  final StackTrace stackTrace;

  @override
  String toString() => "${describeIdentity(this)}("
      "error: $error, "
      "stackTrace: $stackTrace)";
}
