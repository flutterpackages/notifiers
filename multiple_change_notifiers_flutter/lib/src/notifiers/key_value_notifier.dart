part of "../../multiple_change_notifiers.dart";

/// A [ChangeNotifier] that keeps track of it's listeners by
/// assigning each listener a key of type `K`.
///
/// This is not a subtype of [ChangeNotifier], because the
/// [ChangeNotifier.addListener] and [ChangeNotifier.removeListener] methods
/// require a different signature in a [KeyValueNotifer].
class KeyValueNotifer<K, V>
    with DisposableMixin
    implements KeyValueListenable<K, V> {
  /// Initialize this [KeyValueNotifer] with an [AdvancedValueNotifier]
  /// that is initialized with [value].
  factory KeyValueNotifer(V value) =>
      KeyValueNotifer.withNotifier(AdvancedValueNotifier(value));

  /// Initialize this [KeyValueNotifer] with an [EmptyValueNotifier.empty()].
  factory KeyValueNotifer.empty() =>
      KeyValueNotifer.withNotifier(EmptyValueNotifier.empty());

  /// Initialize this [KeyValueNotifer] with [notifier]
  /// as it's internal [ValueNotifierBase].
  KeyValueNotifer.withNotifier(ValueNotifierBase<V> notifier)
      : _listeners = const {},
        _notifier = notifier {
    _notifier.addValueListener(_internalValueListener);
  }

  Map<K, _Listener<V>> _listeners;
  final ValueNotifierBase<V> _notifier;

  @override
  bool? get isEmpty {
    if (_notifier is! EmptyValueNotifier<V>) return null;
    return _notifier.isEmpty;
  }

  @override
  bool? get isNotEmpty {
    if (_notifier is! EmptyValueNotifier<V>) return null;
    return _notifier.isNotEmpty;
  }

  @override
  bool get hasListeners => _listeners.isNotEmpty;

  // We do not have to worry about the map being modified while iterating,
  // because every time a new listener gets added/removed, _listeners
  // is reassigned to a new Map object.
  @override
  Iterable<K> get keys => UnmodifiableListView(_listeners.keys);

  @override
  Type get notifierType => _notifier.runtimeType;

  /// {@macro src.empty.empty_value_notifier.value}
  @override
  V get value => _notifier.value;

  set value(V newValue) => _notifier.value = newValue;

  void _internalValueListener(V value) {
    for (final item in _listeners.values)
      item.hasVoidCallback ? item.listener() : item.valueListener(value);
  }

  bool _addListener(K key, _Listener<V> listener) {
    final containedKey = _listeners.containsKey(key);

    _listeners = Map.of({
      ..._listeners,
      // Add the new listener
      key: listener,
    });

    return containedKey;
  }

  @override
  bool addListener(K key, VoidCallback listener) =>
      _addListener(key, _Listener(listener));

  @override
  bool addValueListener(K key, ValueCallback<V> listener) =>
      _addListener(key, _Listener.value(listener));

  @override
  bool removeListener(K key) {
    final containedKey = _listeners.containsKey(key);

    _listeners = Map.of({
      for (final item in _listeners.entries)
        // Only add listeners if their key is unequal to the
        // key of the listener you want to remove.
        if (item.key != key) item.key: item.value,
    });

    return containedKey;
  }

  @override
  bool removeValueListener(K key) => removeListener(key);

  /// This method calls `dispose()` on the notifier this [KeyValueNotifer]
  /// was instantiated with.
  ///
  /// {@macro src.disposable.dipose}
  @override
  void dispose() {
    _listeners = const {};
    _notifier.dispose();
  }

  @override
  String toString() => "${describeIdentity(this)}($_notifier)";
}

/// Either [listener] or [valueListener] has to be `null`.
@immutable
class _Listener<T> {
  const _Listener(VoidCallback this._listener) : _valueListener = null;

  const _Listener.value(ValueCallback<T> this._valueListener)
      : _listener = null;

  final VoidCallback? _listener;
  final ValueCallback<T>? _valueListener;

  bool get hasVoidCallback => null != _listener && null == _valueListener;

  bool get hasValueCallback => null == _listener && null != _valueListener;

  VoidCallback get listener {
    if (!hasVoidCallback)
      throw StateError("This _Listener holds a ValueCallback of $T.");

    return _listener!;
  }

  ValueCallback<T> get valueListener {
    if (!hasValueCallback)
      throw StateError("This _Listener holds a VoidCallback.");

    return _valueListener!;
  }

  @override
  int get hashCode =>
      hasVoidCallback ? listener.hashCode : valueListener.hashCode;

  @override
  bool operator ==(Object other) {
    if (other is! _Listener<T>) return false;
    if (other.hasVoidCallback != hasVoidCallback) return false;
    if (other.hasVoidCallback && (other.listener != listener)) return false;
    if (other.valueListener != valueListener) return false;

    return true;
  }
}

/// An interface for a [KeyValueNotifer] that exposes a [value].
///
/// This is not a subtype of [ValueListenable] or [AdvancedValueListenable],
/// because the [Listenable.addListener], [Listenable.removeListener],
/// [AdvancedValueListenable.addValueListener] and
/// [AdvancedValueListenable.removeValueListener] methods require a different
/// signature in a [KeyValueListenable].
abstract class KeyValueListenable<K, V> {
  const KeyValueListenable();

  /// If the internal [NotifierBase] is an [EmptyValueNotifier], you can check
  /// if it is empty.
  ///
  /// Trying to read [isEmpty], when the internal [NotifierBase] is not an
  /// [EmptyValueNotifier] returns `null`.
  bool? get isEmpty;

  /// If the internal [NotifierBase] is an [EmptyValueNotifier], you can check
  /// if it is not empty.
  ///
  /// Trying to read [isEmpty], when the internal [NotifierBase] is not an
  /// [EmptyValueNotifier] returns `null`.
  bool? get isNotEmpty;

  bool get hasListeners;

  Iterable<K> get keys;

  /// Get the [Type] of the underlying [NotifierBase].
  ///
  /// The returned [Type] is a subtype of `NotifierBase<V>`.
  Type get notifierType;

  /// The current value of the object. When the value changes, the
  /// callbacks registered with [addListener] and [addValueListener]
  /// will be invoked.
  V get value;

  /// {@template src.key.key_value_listenable.add_listener}
  /// Returns `true` if [key] was not already used for another
  /// listener.
  ///
  /// If another listener was previously associated with [key], then
  /// the only one gets replaced with [listener] and `false` gets
  /// returned.
  /// {@endtemplate}
  bool addListener(K key, VoidCallback listener);

  /// {@template src.key.key_value_listenable.remove_listener}
  /// Returns `true` if [key] was used for a listener and therefore
  /// successfully removed.
  ///
  /// Returns `false`, if there was no listener associated with [key].
  /// {@endtemplate}
  bool removeListener(K key);

  /// {@macro src.key.key_value_listenable.add_listener}
  bool addValueListener(K key, ValueCallback<V> listener);

  /// {@macro src.key.key_value_listenable.remove_listener}
  bool removeValueListener(K key);
}
