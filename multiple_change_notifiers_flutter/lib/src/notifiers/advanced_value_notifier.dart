part of "../../multiple_change_notifiers.dart";

class AdvancedValueNotifier<T> extends ValueNotifierBase<T>
    with ValueComparatorMixin<T> {
  AdvancedValueNotifier(
    this._value, {
    this.comparator,
  });

  @override
  final EqualityComparator<T>? comparator;

  T _value;

  @override
  T get value => _value;

  @override
  set value(T newValue) {
    if (_compare(_value, newValue)) return;
    _value = newValue;
    notifyListeners();
  }
}

/// An interface for subclasses of [Listenable] that expose a [value].
///
/// This class adds the comfort of listeners which directly
/// expose the current value.
abstract class AdvancedValueListenable<T> extends ValueListenable<T> {
  void addValueListener(ValueCallback<T> listener);

  void removeValueListener(ValueCallback<T> listener);
}

mixin AdvancedValueListenableMixin<T> on ChangeNotifier
    implements AdvancedValueListenable<T>, DisposableMixin {
  List<ValueCallback<T>?>? _valueListeners;
  int? _valueListenersCount;

  void _initializeMixinVariables() {
    if (null != _valueListeners && null != _valueListenersCount) return;

    addListener(_valueListenersHandler);
    _valueListeners = List.filled(0, null);
    _valueListenersCount = 0;
  }

  void _disposeMixinVariables() {
    removeListener(_valueListenersHandler);
    _valueListeners = null;
    _valueListenersCount = null;
  }

  void _valueListenersHandler() {
    final _value = value;
    for (var i = 0; i < _valueListeners!.length; i++) {
      final listener = _valueListeners![i];
      listener?.call(_value);
    }
  }

  @override
  void addValueListener(ValueCallback<T> listener) {
    _initializeMixinVariables();
    assert(null != _valueListeners && null != _valueListenersCount);

    if (0 == _valueListenersCount) {
      _valueListeners = List<ValueCallback<T>?>.filled(1, listener);
      _valueListenersCount = 1;
      return;
    }

    // Increase size of _valueListeners.
    if (_valueListenersCount! >= _valueListeners!.length) {
      final newListeners =
          List<ValueCallback<T>?>.filled(_valueListeners!.length * 2, null);

      for (var i = 0; i < _valueListenersCount!; i++)
        newListeners[i] = _valueListeners![i];

      _valueListeners = newListeners;
    }

    _valueListeners![_valueListenersCount!] = listener;
    _valueListenersCount = _valueListenersCount! + 1;
  }

  @override
  void removeValueListener(ValueCallback<T> listener) {
    _initializeMixinVariables();
    assert(null != _valueListeners && null != _valueListenersCount);

    // Copied from [ChangeNotifer].
    void _removeAt(int index) {
      // The list holding the listeners is not growable for performances
      // reasons. We still want to shrink this list if a lot of listeners
      // have been added and then removed outside a notifyListeners iteration.
      // We do this only when the real number of listeners is half the length
      // of our list.
      _valueListenersCount = _valueListenersCount! - 1;

      if (_valueListenersCount! * 2 <= _valueListeners!.length) {
        final newListeners =
            List<ValueCallback<T>?>.filled(_valueListenersCount!, null);

        // Listeners before the index are at the same place.
        for (var i = 0; i < index; i++) newListeners[i] = _valueListeners![i];

        // Listeners after the index move towards the start of the list.
        for (var i = index; i < _valueListenersCount!; i++)
          newListeners[i] = _valueListeners![i + 1];

        _valueListeners = newListeners;
      } else {
        // When there are more listeners than half the length of the list, we
        // only shift our listeners, so that we avoid to reallocate memory for
        // the whole list.
        for (var i = index; i < _valueListenersCount!; i++)
          _valueListeners![i] = _valueListeners![i + 1];

        _valueListeners![_valueListenersCount!] = null;
      }
    }

    for (var i = 0; i < _valueListenersCount!; i++) {
      final _listener = _valueListeners![i];

      if (_listener == listener) {
        _removeAt(i);
        break;
      }
    }
  }

  @override
  void dispose() {
    _disposeMixinVariables();
    assert(null == _valueListeners && null == _valueListenersCount);

    super.dispose();
  }
}
