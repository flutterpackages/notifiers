part of "../../multiple_change_notifiers.dart";

class StreamSubscriptionNotifier<T> extends ValueNotifierBase<T>
    implements StreamSubscriptionListenable<T> {
  /// A [StreamSubscriptionNotifier] exposes a [StreamSubscription] to
  /// a [ChangeNotifier].
  ///
  /// Everytime the [StreamSubscription.onData] callback gets called
  /// the [value] of this is set to the bew data from
  /// [StreamSubscription.onData].
  ///
  /// When the [StreamSubscription.onError] callback gets called and
  /// [ignoreError] and [cancelOnError] are `false` this
  /// [StreamSubscriptionNotifier] removes all it's handlers. Afterwards this
  /// [StreamSubscriptionNotifier] can no longer emit updates because it does
  /// not listen to the [StreamSubscription] anymore.
  ///
  /// When the [StreamSubscription.onDone] callback gets called and
  /// [cancelOnDone] is `false` this [StreamSubscriptionNotifier] removes all
  /// it's handlers. Afterwards this [StreamSubscriptionNotifier] can no longer
  /// emit updates because it does not listen to the [StreamSubscription]
  /// anymore.
  StreamSubscriptionNotifier(
    StreamSubscription<T> this._sub, {
    this.ignoreError = false,
    this.cancelOnDispose = true,
    this.cancelOnError = false,
    this.cancelOnDone = false,
  })  : _onErrorNotifier = EmptyValueNotifier.empty(),
        _isDoneNotifier = AdvancedValueNotifier(false),
        _isCancelledNotifier = AdvancedValueNotifier(false),
        _hasValue = false,
        assert(
            !ignoreError || (ignoreError && !cancelOnError),
            "cancelOnError cannot be true, "
            "if you want to ignore incoming errors.") {
    _sub!
      ..onError(_onError)
      ..onDone(_onDone)
      ..onData(_onData);
  }

  /// Register a [StreamSubscription] on [stream] and instantiate a
  /// [StreamSubscriptionNotifier] from the registered [StreamSubscription].
  factory StreamSubscriptionNotifier.stream(
    Stream<T> stream, {
    bool ignoreError = false,
    bool cancelOnDispose = true,
    bool cancelOnError = false,
    bool cancelOnDone = false,
  }) =>
      StreamSubscriptionNotifier(
        stream.sub,
        ignoreError: ignoreError,
        cancelOnDispose: cancelOnDispose,
        cancelOnError: cancelOnError,
        cancelOnDone: cancelOnDone,
      );

  StreamSubscription<T>? _sub;

  late T _value;
  bool _hasValue;

  final EmptyValueNotifier<StreamError> _onErrorNotifier;
  final AdvancedValueNotifier<bool> _isCancelledNotifier;
  final AdvancedValueNotifier<bool> _isDoneNotifier;

  @override
  final bool ignoreError;

  @override
  final bool cancelOnDispose;

  @override
  final bool cancelOnError;

  @override
  final bool cancelOnDone;

  @override
  bool get hasValue => _hasValue;

  @override
  T get value {
    if (!_hasValue)
      throw StateError("You cannot get the last value the stream subscription "
          "emitted, because there is none. Wait until hasLastValue "
          "turns to true");

    return _value;
  }

  @override
  set value(T newValue) => throw StateError(
        "You cannot manually change the value of a StreamSubscriptionNotifier.",
      );

  @override
  EmptyValueListenable<StreamError> get onError => _onErrorNotifier;

  @override
  AdvancedValueListenable<bool> get isDone => _isDoneNotifier;

  @override
  AdvancedValueListenable<bool> get isCancelled => _isCancelledNotifier;

  @override
  bool get hadError => onError.isNotEmpty;

  void _removeSub() {
    _sub!
      ..onError(null)
      ..onDone(null)
      ..onData(null);
    _sub = null;
  }

  void _onData(T data) {
    _hasValue = true;
    _value = data;
    notifyListeners();
  }

  Future<void> _onError(Object error, StackTrace stackTrace) async {
    if (!ignoreError) cancelOnError ? await cancel() : _removeSub();
    _onErrorNotifier.value = StreamError._(
      error: error,
      stackTrace: stackTrace,
    );
  }

  Future<void> _onDone() async {
    cancelOnDone ? await cancel() : _removeSub();
    _isDoneNotifier.value = true;
  }

  /// Cancel the underlying [StreamSubscription].
  ///
  /// Returns `true` if the [StreamSubscription] was successfully cancelled,
  /// or if it was already cancelled.
  ///
  /// Returns `true` if [isDone] and [cancelOnDone] are `true`.
  ///
  /// Returns `true` if [hadError] and [cancelOnError] are `true`.
  Future<bool> cancel() async {
    if (isCancelled.value) return true;
    if (isDone.value) return cancelOnDone;
    if (hadError) return cancelOnError;

    _isCancelledNotifier.value = true;

    await _sub!.cancel();
    _sub = null;

    return true;
  }

  @override
  Future<void> dispose() async {
    super.dispose();
    if (cancelOnDispose) await cancel();
  }

  @override
  String toString() => "${describeIdentity(this)}("
      "_sub: $_sub, "
      "hasValue: $hasValue, "
      "${hasValue ? "value: $value, " : ""}"
      "isDone: $isDone, "
      "isCancelled: $isCancelled, "
      "hadError: $hadError)";
}

abstract class StreamSubscriptionListenable<T>
    extends AdvancedValueListenable<T> {
  /// `True` if this [StreamSubscriptionNotifier] should ignore it if the
  /// underlying [StreamSubscription] emits an error.
  ///
  /// [onError] will still get updates, but the [StreamSubscriptionNotifier]
  /// will not cancel the [StreamSubscription], nor will it remove
  /// it's listeners.
  bool get ignoreError;

  /// `True` if the supplied [StreamSubscription] should
  /// get called once [StreamSubscriptionNotifier.dispose] gets called.
  bool get cancelOnDispose;

  /// `True` if the supplied [StreamSubscription] should
  /// get called once an error gets emitted.
  bool get cancelOnError;

  /// `True` if the supplied [StreamSubscription] should
  /// get called once it is done.
  bool get cancelOnDone;

  /// If `true` then [value] can successfully be read.
  ///
  /// If `false` then [value] throws a [StateError] when read.
  bool get hasValue;

  /// The value of a [StreamSubscriptionNotifier] is `read-only`, it can only be
  /// modified by the [StreamSubscription.onData] callback. Use this getter to
  /// read the most recent value the [StreamSubscription], that was used to
  /// initialize this [StreamSubscriptionNotifier], emitted.
  ///
  /// Can only be read if [hasValue] is `true`.
  ///
  /// It is `always safe` to read [value] in a listener.
  @override
  T get value;

  /// Get the most recent [StreamError] the [StreamSubscription] emitted.
  ///
  /// The value of [onError] gets updated right after the error handling
  /// routine of this [StreamSubscriptionNotifier].
  EmptyValueListenable<StreamError> get onError;

  /// Whether the underlying [StreamSubscription] is done or not.
  ///
  /// {@template src.stream_subscription.stream_subscription_listenable.done}
  /// Once the [StreamSubscription] got cancelled, all references to it are
  /// removed from this [StreamSubscriptionNotifier] and no more events
  /// will be received.
  /// {@endtemplate}
  AdvancedValueListenable<bool> get isDone;

  /// `True` if the underlying [StreamSubscription] got cancelled by this
  /// [StreamSubscriptionNotifier].
  ///
  /// {@macro src.stream_subscription.stream_subscription_listenable.done}
  AdvancedValueListenable<bool> get isCancelled;

  /// `True` if the underlying [StreamSubscription] emitted an error.
  ///
  /// {@macro src.stream_subscription.stream_subscription_listenable.done}
  bool get hadError;
}
