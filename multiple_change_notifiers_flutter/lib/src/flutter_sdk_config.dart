import "flutter_sdk/change_notifier.dart";

typedef FlutterErrorFactory = Error Function(String message);
typedef FlutterErrorHandler = void Function(Object exception, StackTrace stack);
typedef FlutterObjectCreatedDispatcher = void Function({
  required String library,
  required String className,
  required Object object,
});
typedef FlutterObjectDisposedDispatcher = void Function({
  required Object object,
});

//

final class FlutterSdkUtils implements LibConfig {
  FlutterSdkUtils._() : _args = const DartSdkLibConfig();

  static FlutterSdkUtils? _instance;
  // ignore: prefer_constructors_over_static_methods
  static FlutterSdkUtils get instance => _instance ??= FlutterSdkUtils._();

  LibConfig _args;
  set args(LibConfig value) => _args = value;

  @override
  bool get kFlutterMemoryAllocationsEnabled =>
      _args.kFlutterMemoryAllocationsEnabled;

  @override
  FlutterErrorHandler get onListenerError => _args.onListenerError;

  @override
  FlutterErrorFactory get flutterErrorFactory => _args.flutterErrorFactory;

  @override
  FlutterObjectCreatedDispatcher get dispatchObjectCreated =>
      _args.dispatchObjectCreated;

  @override
  FlutterObjectDisposedDispatcher get dispatchObjectDisposed =>
      _args.dispatchObjectDisposed;
}

//

/// Configuration arguments for the library for when this package
/// is used with the flutter sdk or just alone with the dart sdk.
abstract interface class LibConfig {
  bool get kFlutterMemoryAllocationsEnabled;

  /// Called whenever a listener from a [ChangeNotifier] triggers an error.
  ///
  /// When using this package with the flutter sdk, then this
  /// callback should be set like this:
  ///
  /// ```dart
  /// FlutterSdkConfig.onListenerError = (exception, stack) {
  ///   FlutterError.reportError(FlutterErrorDetails(
  ///     exception: exception,
  ///     stack: stack,
  ///     library: 'foundation library',
  ///     context: ErrorDescription(
  ///         'while dispatching notifications for $runtimeType'),
  ///     informationCollector: () => <DiagnosticsNode>[
  ///       DiagnosticsProperty<ChangeNotifier>(
  ///         'The $runtimeType sending notification was',
  ///         this,
  ///         style: DiagnosticsTreeStyle.errorProperty,
  ///       ),
  ///     ],
  ///   ));
  /// }
  /// ```
  FlutterErrorHandler get onListenerError;

  /// A function that creates `FlutterError` instances with the provided
  /// message.
  FlutterErrorFactory get flutterErrorFactory;

  /// The method `dispatchObjectCreated` from
  /// `FlutterMemoryAllocations.instance`.
  FlutterObjectCreatedDispatcher get dispatchObjectCreated;

  /// The method `dispatchObjectDisposed` from
  /// `FlutterMemoryAllocations.instance`.
  FlutterObjectDisposedDispatcher get dispatchObjectDisposed;
}

final class FlutterSdkLibConfig implements LibConfig {
  const FlutterSdkLibConfig({
    required this.kFlutterMemoryAllocationsEnabled,
    required this.onListenerError,
    required this.flutterErrorFactory,
    required this.dispatchObjectCreated,
    required this.dispatchObjectDisposed,
  });

  @override
  final bool kFlutterMemoryAllocationsEnabled;

  @override
  final FlutterErrorHandler onListenerError;

  @override
  final FlutterErrorFactory flutterErrorFactory;

  @override
  final FlutterObjectCreatedDispatcher dispatchObjectCreated;

  @override
  final FlutterObjectDisposedDispatcher dispatchObjectDisposed;
}

final class DartSdkLibConfig implements LibConfig {
  const DartSdkLibConfig({
    FlutterErrorHandler? onListenerError,
  }) : _onListenerError = onListenerError;

  final FlutterErrorHandler? _onListenerError;

  @override
  bool get kFlutterMemoryAllocationsEnabled => false;

  @override
  FlutterObjectCreatedDispatcher get dispatchObjectCreated {
    return ({
      required String library,
      required String className,
      required Object object,
    }) {};
  }

  @override
  FlutterObjectDisposedDispatcher get dispatchObjectDisposed {
    return ({required Object object}) {};
  }

  @override
  FlutterErrorFactory get flutterErrorFactory {
    // A FlutterError implements the AssertionError type, therefore
    // we use AssertionErrors when we cannot use the Flutter SDK.
    //
    // See https://api.flutter.dev/flutter/foundation/FlutterError-class.html
    return AssertionError.new;
  }

  @override
  FlutterErrorHandler get onListenerError {
    if (null != _onListenerError) return _onListenerError;
    return (err, st) {
      // ignore: only_throw_errors
      throw err;
    };
  }
}
