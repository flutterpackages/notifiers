import "dart:async";

import "package:dartx/dartx.dart";
import "package:flutter_test/flutter_test.dart";
import "package:multiple_change_notifiers_flutter/multiple_change_notifiers.dart";
import "package:random_string/random_string.dart";

void main() {
  group("Tests for $AdvancedValueNotifier:", () {
    test("Add 1 value listener and listen for a result.", () async {
      const updateValue = "I Like Cookies";

      final completer = Completer<String>();
      final notifier = AdvancedValueNotifier("initial");

      void valueListener(String value) {
        completer.complete(value);
      }

      notifier
        ..addValueListener(valueListener)
        ..value = updateValue;

      expect(await completer.future, updateValue);

      notifier.removeValueListener(valueListener);
    });

    test("Add 10 value listener and listen for a result.", () async {
      const result = "I Like Cookies";

      final notifier = AdvancedValueNotifier("initial");
      final listeners = List.generate(10, (index) => Completer<String>())
          .associateWith((e) => e.complete)
        ..forEach((key, value) => notifier.addValueListener(value));

      notifier.value = result;

      for (final item in listeners.keys) expect(await item.future, result);

      listeners.forEach((key, value) => notifier.removeValueListener(value));
    });

    test("Add 10 value listener and listen for 50 results.", () async {
      final notifier = AdvancedValueNotifier("initial");

      for (var i = 0; i < 50; i++) {
        final result = randomString(10);

        // Gotta create listeners each iteration, because
        // a Completer is useless after Completer.complete has been
        // called.
        final listeners = List.generate(10, (index) => Completer<String>())
            .associateWith((e) => e.complete)
          ..forEach((key, value) => notifier.addValueListener(value));

        notifier.value = result;

        for (final completer in listeners.keys)
          expect(await completer.future, result);

        listeners.forEach((key, value) => notifier.removeValueListener(value));
      }
    });

    test(
        "Add 10 value listener and 10 normal listener "
        "and listen for 50 results.", () async {
      final notifier = AdvancedValueNotifier("initial");

      for (var i = 0; i < 50; i++) {
        final result = randomString(10);

        // Gotta create listeners each iteration, because
        // a Completer is useless after Completer.complete has been
        // called.
        final valueListeners = List.generate(10, (index) => Completer<String>())
            .associateWith((e) => e.complete)
          ..forEach((key, value) => notifier.addValueListener(value));
        final normalListeners =
            List.generate(10, (index) => Completer<String>())
                .associateWith((e) => () => e.complete(notifier.value))
              ..forEach((key, value) => notifier.addListener(value));

        notifier.value = result;

        for (final completer in [
          ...valueListeners.keys,
          ...normalListeners.keys,
        ]) expect(await completer.future, result);

        valueListeners
            .forEach((key, value) => notifier.removeValueListener(value));
        normalListeners.forEach((key, value) => notifier.removeListener(value));
      }
    });

    test("Custom comparator", () {
      final notifier = AdvancedValueNotifier(
        "initial",
        comparator: (a, b) => false,
      );

      final values = [
        "first",
        "second",
        "second",
        "third",
        "fourth",
      ];
      var index = 0;

      void valueListener(String value) {
        expect(value, values[index]);
      }

      notifier.addValueListener(valueListener);
      for (; index < values.length; index++) {
        notifier.value = values[index];
      }
      notifier.removeValueListener(valueListener);
    });
  });
}
