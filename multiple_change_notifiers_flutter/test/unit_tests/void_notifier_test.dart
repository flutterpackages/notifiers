import "package:flutter_test/flutter_test.dart";
import "package:multiple_change_notifiers_flutter/multiple_change_notifiers.dart";

void main() {
  group("Tests for $VoidNotifier:", () {
    test("Notify listener 5x", () {
      var count = 0;

      void listener() {
        count++;
      }

      final notifier = VoidNotifier();
      notifier.addListener(listener);

      expect(count, 0);

      notifier.notify();
      expect(count, 1);

      notifier.notify();
      expect(count, 2);

      notifier.notify();
      expect(count, 3);

      notifier.notify();
      expect(count, 4);

      notifier.notify();
      expect(count, 5);

      notifier.removeListener(listener);

      notifier.notify();
      expect(count, 5);
    });
  });
}
