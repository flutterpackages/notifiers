import "dart:async";

import "package:dartx/dartx.dart";
import "package:flutter_test/flutter_test.dart";
import "package:multiple_change_notifiers_flutter/multiple_change_notifiers.dart";
import "package:random_string/random_string.dart";

void main() {
  group("Tests for KeyValueNotifier<int, String>:", () {
    test("Add 1 valueListener and listen for a result.", () async {
      const updateValue = "I Like Cookies";

      final completer = Completer<String>();
      final notifier = KeyValueNotifer<int, String>.empty();

      void valueListener(String value) {
        completer.complete(value);
      }

      notifier
        ..addValueListener(0, valueListener)
        ..value = updateValue;

      expect(await completer.future, updateValue);

      notifier.removeValueListener(0);
    });

    test(
      "Instantiate an KeyValueNotifer.empty() "
      "and read it's value without setting it. "
      "This should throw a StateError.",
      () async {
        final notifier = KeyValueNotifer<int, String>.empty();
        expect(
          // We have to use a function instead of just the getter
          // for our actual value, due to the way throwsA detects
          // an Error.
          () => notifier.value,
          throwsA(isA<StateError>()),
        );
      },
    );

    test("Calling toString on an empty KeyValueNotifer.", () {
      KeyValueNotifer<int, String>.empty().toString();
    });

    test("Add 1 value listener and listen for a result.", () async {
      const updateValue = "I Like Cookies";

      final completer = Completer<String>();
      final notifier = KeyValueNotifer<int, String>("initial");

      void valueListener(String value) {
        completer.complete(value);
      }

      notifier
        ..addValueListener(89, valueListener)
        ..value = updateValue;

      expect(await completer.future, updateValue);

      notifier.removeValueListener(89);
    });

    test("Add 10 value listener and listen for a result.", () async {
      const result = "I Like Cookies";

      final notifier = KeyValueNotifer<int, String>("initial");
      final listeners = List.generate(10, (index) => index)
          .associateWith((element) => Completer<String>())
        ..forEach(
          (key, value) => notifier.addValueListener(key, value.complete),
        );

      notifier.value = result;

      for (final item in listeners.values) expect(await item.future, result);

      listeners.keys.forEach(notifier.removeValueListener);
    });

    test("Add 10 value listener and listen for 50 results.", () async {
      final notifier = KeyValueNotifer<int, String>.withNotifier(
        AdvancedValueNotifier("nonaned"),
      );

      for (var i = 0; i < 50; i++) {
        final result = randomString(10);

        // Gotta create listeners each iteration, because
        // a Completer is useless after Completer.complete has been
        // called.
        final listeners = List.generate(10, (index) => index)
            .associateWith((element) => Completer<String>())
          ..forEach(
            (key, value) => notifier.addValueListener(key, value.complete),
          );

        notifier.value = result;

        for (final item in listeners.values) expect(await item.future, result);

        listeners.keys.forEach(notifier.removeValueListener);
      }
    });

    test(
        "Add 10 value listener and 10 normal listener "
        "and listen for 50 results.", () async {
      final notifier = KeyValueNotifer<int, String>("initial");

      for (var i = 0; i < 50; i++) {
        final result = randomString(10);

        // Gotta create listeners each iteration, because
        // a Completer is useless after Completer.complete has been
        // called.
        final valueListeners = List.generate(10, (index) => index)
            .associateWith((_) => Completer<String>())
          ..forEach(
            (key, value) => notifier.addValueListener(key, value.complete),
          );
        final normalListeners = List.generate(10, (index) => index + 100)
            .associateWith((_) => Completer<void>())
          ..forEach((key, value) => notifier.addListener(key, value.complete));

        notifier.value = result;

        for (final completer in [
          ...valueListeners.values,
          ...normalListeners.values,
        ]) {
          if (completer is Completer<String>) {
            final completedResult = await completer.future;
            expect(completer.isCompleted, isTrue);
            expect(completedResult, result);
            continue;
          }

          await completer.future;
          expect(completer.isCompleted, isTrue);
        }

        valueListeners.keys.forEach(notifier.removeValueListener);
        normalListeners.keys.forEach(notifier.removeValueListener);
      }
    });

    test(
        "Instantiate KeyValueNotifer with EmptyValueNotifiers "
        "and check the KeyValueNotifer's state.", () async {
      final notifier = KeyValueNotifer<int, String>.empty();

      expect(notifier.isEmpty, true);
      expect(notifier.isNotEmpty, false);
      expect(
        // We have to use a function instead of just the getter
        // for our actual value, due to the way throwsA detects
        // an Error.
        () => notifier.value,
        throwsA(isA<StateError>()),
      );

      const value = "initial";
      final notifier2 =
          KeyValueNotifer<int, String>.withNotifier(EmptyValueNotifier(value));

      expect(notifier2.isEmpty, false);
      expect(notifier2.isNotEmpty, true);
      expect(notifier2.value, value);
    });

    test(
        "Instantiate KeyValueNotifer with AdvancedValueNotifiers "
        "and check the KeyValueNotifer.isEmpty and KeyValueNotifer.isNotEmpty.",
        () async {
      const value = "abcde";
      final notifier = KeyValueNotifer<int, String>.withNotifier(
        AdvancedValueNotifier(value),
      );

      expect(notifier.isEmpty, null);
      expect(notifier.isNotEmpty, null);
      expect(notifier.value, value);
    });

    test(
      "Instantiate KeyValueNotifer with StreamSubscriptionNotifier.",
      () async {
        final values = List.generate(20, (index) => randomString(10));
        var listenerIndex1 = 0;
        var listenerIndex2 = 0;

        final controller = StreamController<String>();

        final notifier = KeyValueNotifer<int, String>.withNotifier(
          StreamSubscriptionNotifier(controller.stream.sub),
        );
        notifier
          ..addValueListener(10, (value) {
            expect(value, values[listenerIndex1++]);
          })
          ..addListener(99, () {
            expect(notifier.value, values[listenerIndex2++]);
          });

        values.forEach(controller.add);

        await controller.close();

        expect(
          () => notifier.value = "nona",
          throwsA(isA<StateError>()),
        );
      },
    );

    test("Call dispose on an empty KeyValueNotifer-", () {
      final n = KeyValueNotifer.empty();
      n.dispose();
    });
  });
}
