#! /bin/dart

import "dart:async";
import "dart:io";

import "package:my_utility/dartx.dart";
import "package:my_utility/extensions.dart";
import "package:my_utility/printers.dart";
import "package:path/path.dart" as lib_path;
import "package:yaml/yaml.dart";
import "package:yaml_edit/yaml_edit.dart";

final rootDir = File.fromUri(Platform.script).parent.parent.parent;

Future<void> main() async {
  final targetDir = rootDir.directory("multiple_change_notifiers_flutter");
  final sourceDir = rootDir.directory("multiple_change_notifiers");

  // Delete old targetDir.
  await _deleteIfExists(targetDir);

  // Clean source project before copying.
  print("Cleaning '${sourceDir.path}' ...");
  await _deleteIfExists(sourceDir.directory(".dart_tool"));
  await _deleteIfExists(sourceDir.file("pubspec.lock"));

  // Copy contents of sourceDir into targetDir.
  print("Copying '${sourceDir.path}' to '${targetDir.path}' ...");
  await sourceDir.copyRecursively(targetDir);

  // Modify pubspec.yaml in targetDir.
  print("Modifying pubspec.yaml...");
  final pubspecFile = targetDir.file("pubspec.yaml");
  final pubspec = YamlEditor(await pubspecFile.readAsString());

  pubspec.update(["name"], targetDir.name);
  final oldRepoUrl = Uri.parse(pubspec.parseAt(["repository"]).toString());
  final newRepoUrl = Uri(
    scheme: oldRepoUrl.scheme,
    host: oldRepoUrl.host,
    pathSegments: [
      ...oldRepoUrl.pathSegments.dropLast(1),
      targetDir.name,
    ],
  );
  pubspec.update(["repository"], newRepoUrl.toString());

  // Add Flutter SDK to dependencies.
  final oldDeps = pubspec.parseAt(["dependencies"]) as YamlMap;
  pubspec.update(
    [
      "dependencies",
    ],
    {
      "flutter": {"sdk": "flutter"},
      ...oldDeps,
    },
  );

  // Add Flutter test SDK to dev_dependencies.
  final oldDevDeps = pubspec.parseAt(["dev_dependencies"]) as YamlMap;
  pubspec.update(
    ["dev_dependencies"],
    {
      "flutter_test": {"sdk": "flutter"},
      for (final MapEntry(key: k, value: v) in oldDevDeps.entries)
        if ("test" != k) k: v,
    },
  );

  // Add Flutter SDK to environment restrictions.
  final oldEnv = pubspec.parseAt(["environment"]) as YamlMap;
  pubspec.update(
    ["environment"],
    {
      ...oldEnv,
      "flutter": ">=1.17.0",
    },
  );

  await pubspecFile.writeAsString(pubspec.toString());

  // Adjust .gitignore for Flutter.
  print("Adjusting .gitignore ...");
  targetDir.file(".gitignore").writeAsStringSync("""
# Miscellaneous
*.class
*.log
*.pyc
*.swp
.DS_Store
.atom/
.buildlog/
.history
.svn/
migrate_working_dir/

# IntelliJ related
*.iml
*.ipr
*.iws
.idea/

# The .vscode folder contains launch configuration and tasks you configure in
# VS Code which you may wish to be included in version control, so this line
# is commented out by default.
#.vscode/

# Flutter/Dart/Pub related
# Libraries should not include pubspec.lock, per https://dart.dev/guides/libraries/private-files#pubspeclock.
/pubspec.lock
**/doc/api/
.dart_tool/
build/""");

  print("Modifying library directives ...");
  await for (final item in targetDir.list(recursive: true)) {
    if (item is! File || ".dart" != item.extension) continue;
    var data = await item.readAsString();

    data = data.replaceAll(
      "library ${sourceDir.name};",
      "library ${targetDir.name};",
    );

    // Replace imports from "package:multiple_change_notifiers" with
    // "package:multiple_change_notifiers_flutter".
    data = data.replaceAllMapped(
      RegExp("import ([\"'])package:${sourceDir.name}\\/(.+)\\1;"),
      (match) {
        final relPath = match.group(2)!;
        return 'import "package:${targetDir.name}/$relPath";';
      },
    );

    data = data.replaceAll(
      RegExp("import ([\"'])package:test/test\\.dart\\1;"),
      'import "package:flutter_test/flutter_test.dart";',
    );

    await item.writeAsString(data);
  }

  print("Modifying Flutter SDK exports ...");
  final flutterSdkDir =
      Directory(lib_path.join(targetDir.path, "lib", "src", "flutter_sdk"));
  await flutterSdkDir.file("object.dart").writeAsString(
    """
export "package:flutter/foundation.dart" show objectRuntimeType;
""",
    flush: true,
  );
  await flutterSdkDir.file("diagnostics.dart").writeAsString(
    """
export "package:flutter/foundation.dart" show shortHash, describeIdentity;
""",
    flush: true,
  );
  await flutterSdkDir.file("change_notifier.dart").writeAsString(
    """
export "package:flutter/foundation.dart"
    show
        VoidCallback,
        Listenable,
        ValueListenable,
        ChangeNotifier,
        ValueNotifier;
""",
    flush: true,
  );

  print("Getting packages ...");
  _checkProcRes(
    await _runFlutterProc(
      ["pub", "get"],
      workingDirectory: targetDir.path,
    ),
  );

  print("Fixing linter issues ...");
  _checkProcRes(
    await Process.run(
      Platform.executable,
      ["fix", "--code=directives_ordering", "--apply"],
      workingDirectory: targetDir.path,
      runInShell: true,
    ),
  );

  print("Running tests ...");
  final proc = await _spawnFlutterProc(
    ["test"],
    workingDirectory: targetDir.path,
  );
  unawaited(stdout.addStream(proc.stdout));
  unawaited(stderr.addStream(proc.stderr));
  if (0 != await proc.exitCode) {
    printError("Some tests failed.");
    exit(1);
  }

  printInfo("Finished !");
}

Future<void> _deleteIfExists(FileSystemEntity entity) async {
  if (!await entity.exists()) return;
  await entity.delete(recursive: true);
}

void _checkProcRes(ProcessResult res) {
  if (0 == res.exitCode) return;
  printError(res.stderr);
  exit(1);
}

Future<Process> _spawnFlutterProc(
  List<String> arguments, {
  String? workingDirectory,
}) async {
  var res = await Process.run("flutter", ["--version"], runInShell: true);
  if (0 == res.exitCode)
    return Process.start(
      "flutter",
      arguments,
      workingDirectory: workingDirectory,
      runInShell: true,
    );

  res = await Process.run("fvm", ["--version"], runInShell: true);
  if (0 == res.exitCode)
    return Process.start(
      "fvm",
      ["flutter", ...arguments],
      workingDirectory: workingDirectory,
      runInShell: true,
    );

  printError("No Flutter executable found");
  exit(1);
}

Future<ProcessResult> _runFlutterProc(
  List<String> arguments, {
  String? workingDirectory,
}) async {
  final proc = await _spawnFlutterProc(
    arguments,
    workingDirectory: workingDirectory,
  );

  final stdout = systemEncoding.decodeStream(proc.stdout);
  final stderr = systemEncoding.decodeStream(proc.stderr);
  final code = await proc.exitCode;

  return ProcessResult(proc.pid, code, await stdout, await stderr);
}
